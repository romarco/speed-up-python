# Speed up Python with hardware accelerators

The [notebooks](notebooks) folder contains the files to reproduce the examples presented in [slides/pycon2023.pdf](slides/pycon2023.pdf).

## Use a python environment

Whether you use [`conda`](https://www.anaconda.com/) or [`venv`](https://docs.python.org/3/library/venv.html), install the dependencies needed to run the notebooks with the following command:

```bash
pip install -r requirements.txt
```
